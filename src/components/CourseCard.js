import {useState,useEffect} from 'react';
import {Card,Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';


export default function CourseCard({course}) {

	console.log(course);
	console.log(typeof course);

	const {_id,name,description,price} = course;


	// Use the state hook for this component to be able to store its state
		    // States are used to keep track of information related to individual components
		    // Syntax
		        // const [getter, setter] = useState(initialGetterValue);

	// const [count,setCount] = useState(0);

	// const [seats,setSeats] = useState(30);

	// // Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element
	// console.log(useState(0));

	// function enroll() {

	// 	// // console.log(`seats:${seat}`);
	// 	// // if(seat == 0){
	// 	// // 	alert("No more seats");
	// 	// // }else {
	// 	// // 	setCount(count+1);
	// 	// // 	setSeat(seat-1);
	// 	// // } 
	// 	// // /*setCount(count+1);
	// 	// // setSeat(seat-1)*/
	// 	// console.log(`enrolless: ${count}`);

	// 	 if (seats > 0) {
    //         setCount(count + 1);
    //         // console.log('Enrollees: ' + count);
    //         setSeats(seats - 1);
    //         // console.log('Seats: ' + seats);
    //     } 

	// }

	// useEffect(() => {
    //     if(seats === 0){
    //         alert("No more seats available")
    //     }
    // }, [seats])




	return (

		// <Row>
		// 	<Col >
				<Card className="courseCard p-3">
					
					<Card.Body>
						<Card.Title>
							<h2>{ name }</h2>
						</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>
							{description}	
						</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>PHP {price}</Card.Text>
						{/*//<Card.Text>Enrolless: {count}</Card.Text>
						<Card.Text>Seats: {seats}</Card.Text>*/}

						<Link className="btn btn-primary" to={`/courses/${_id}`}>Enroll</Link>

					</Card.Body>
				</Card>
/*
			</Col>
		</Row>*/
	)
}