import { useState,useEffect,useContext } from 'react';
import { Container, Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, Link ,useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function CourseView() {

	const {courseId} = useParams();

	const navigate = useNavigate();

	const {user} = useContext(UserContext);
	console.log(courseId);
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const enroll = (courseId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`,{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization : `Bearer ${localStorage.getItem('token')}`
			},
			body : JSON.stringify({
					courseId : courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data == true){
				Swal.fire({
    			  icon: 'success',
    			  title: 'Enroll Successfull',
    			  text: 'Welcome to Zuitt'
    			  
    			})

    			navigate("/courses");
			} else {
				Swal.fire({
    			  icon: 'error',
    			  title: 'Something went wrong',
    			  text: 'Please try again'
    			  
    			})
			}
		})
	}

	useEffect(()=>{
		console.log(courseId);
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
			.then(res => res.json())
			.then(data => {
				console.log(data);
				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
			})
	})

	return (

		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{ name }</Card.Title>
							<Card.Subtitle>Description</Card.Subtitle>
							<Card.Text>{ description }</Card.Text>
							<Card.Subtitle>Price</Card.Subtitle>
							<Card.Text>Php { price }</Card.Text>
							<Card.Subtitle>Class Schedule</Card.Subtitle>
							<Card.Text>8am-5pm</Card.Text>
							{
								(user.id) ?
									<Button variant="primary" onClick={()=> enroll(courseId) }>Enroll</Button>
								:

									<Link className="btn btn-danger btn-block" to="/login">Log in to Enroll</Link>
							}
							
						</Card.Body>
					</Card>
					
				</Col>
			</Row>
			
		</Container>

	)
} 